require 'rest-client'
require 'json'

class ExchangeService
  
  def initialize(source_currency, target_currency, amount)
    @source_currency = source_currency
    @target_currency = target_currency
    @amount = amount.to_f
  end

  def perform
    return @amount if @source_currency == @target_currency
    has_btc = @source_currency == "BTC" || @target_currency == "BTC"
    has_btc ? get_bitcoin : get_regular_currency
  end

  private

  def get_regular_currency

    begin
      exchange_api_url = Rails.application.credentials[Rails.env.to_sym][:currency_api_url]
      exchange_api_key = Rails.application.credentials[Rails.env.to_sym][:currency_api_key]
      url = "#{exchange_api_url}?token=#{exchange_api_key}&currency=#{@source_currency}/#{@target_currency}"
      res = RestClient.get url
      value = JSON.parse(res.body)['currency'][0]['value'].to_f

      value * @amount
    rescue RestClient::ExceptionWithResponse => e
      e.response
    end

  end

  def get_bitcoin

    begin

      has_usd = @source_currency == "USD" || @target_currency == "USD"
      amount = @amount
      
      res_btc = RestClient.get "https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD"
      usd_btc = JSON.parse(res_btc.body)['USD'].to_f

      # has_usd ? (amount * usd_btc) : 0

      if has_usd
        return @target_currency == "USD" ? (usd_btc * @amount) : (@amount / usd_btc)
      end

      @amount = 1

      if @source_currency == "BTC"
        @source_currency = @target_currency
        @target_currency = "USD"
        usd_btc = (1/usd_btc)
      else
        @target_currency = @source_currency
        @source_currency = "USD"
      end

      usd_target = get_regular_currency
      # return usd_target
      return (amount / usd_btc / usd_target)
    rescue RestClient::ExceptionWithResponse => e
      e.response
    end
  end

end


