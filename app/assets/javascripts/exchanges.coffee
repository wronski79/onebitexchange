# o script aguarda/captura o evento de submissão do formulario
# e ele mesmo chama o /convert
# completa comentario para deploy
# mais um comentario

global_timeout = null

call_conversion = ->
  global_timeout = null
  $.ajax '/convert',
    type: 'GET'
    dataType: 'json'
    data: {
            source_currency: $("#source_currency").val(),
            target_currency: $("#target_currency").val(),
            amount: $("#amount").val()
          }
    error: (jqXHR, textStatus, errorThrown) ->
      alert textStatus
    success: (data, text, jqXHR) ->
      $("#result").val(data.value)

swap_currency = ->
  temp_currency = $("#source_currency").val()
  $("#source_currency").val($("#target_currency").val())
  $("#target_currency").val(temp_currency)


$(document).ready ->

  $("#amount").keyup (e) ->
    if global_timeout != null
      clearTimeout(global_timeout)
    global_timeout = setTimeout(call_conversion ,500)

  $("#source_currency").change ->
    call_conversion() if $("#amount").val() != ""
  
  $("#target_currency").change ->
    call_conversion() if $("#amount").val() != ""
  
  $("#swap_currency").click ->
    swap_currency()
    call_conversion() if $("#amount").val() != ""

    